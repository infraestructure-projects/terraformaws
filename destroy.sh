#!/bin/bash

#Limpieza de Infraestructura en dev
echo "Iniciando limpieza de Infraestructura en dev"
cd  ./aws/environments/dev

terraform destroy -auto-approve

rm -rf .terraform
rm -f terraform.log .terraform.lock.hcl errored.tfstate errored.tfstate terraform.tfstate.backup terraform.tfstate

#Limpieza de Backend en dev
echo "Iniciando limpieza de Backend en dev"
cd ../../../aws-backend/environments/dev

terraform destroy -auto-approve

rm -rf .terraform
rm -f terraform.log .terraform.lock.hcl errored.tfstate errored.tfstate terraform.tfstate.backup terraform.tfstate

#Limpieza de Infraestructura en stage
echo "Iniciando limpieza de Infraestructura en stage"
cd ../../../aws/environments/stage

terraform destroy -auto-approve

rm -rf .terraform
rm -f terraform.log .terraform.lock.hcl errored.tfstate errored.tfstate terraform.tfstate.backup terraform.tfstate

#Limpieza de Backend en stage
echo "Iniciando limpieza de Backend en stage"
cd ../../../aws-backend/environments/stage

terraform destroy -auto-approve

rm -rf .terraform
rm -f terraform.log .terraform.lock.hcl errored.tfstate errored.tfstate terraform.tfstate.backup terraform.tfstate

#Limpieza de Infraestructura en prod
echo "Iniciando limpieza de Infraestructura en prod"
cd ../../../aws/environments/prod

terraform destroy -auto-approve

rm -rf .terraform
rm -f terraform.log .terraform.lock.hcl errored.tfstate errored.tfstate terraform.tfstate.backup terraform.tfstate

#Limpieza de Backend en prod
echo "Iniciando limpieza de Backend en prod"
cd ../../../aws-backend/environments/prod

terraform destroy -auto-approve

rm -rf .terraform
rm -f terraform.log .terraform.lock.hcl errored.tfstate errored.tfstate terraform.tfstate.backup terraform.tfstate

echo "Limpieza completa."
