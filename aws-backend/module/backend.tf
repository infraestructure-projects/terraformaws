resource "aws_s3_bucket" "terraform_state" {
    bucket = "tf-state-bucket-j-${var.environment}"
    force_destroy = true
}

resource "aws_dynamodb_table" "terraform_locks" {
    name = "terraform-state-locking-j-${var.environment}"
    billing_mode = "PAY_PER_REQUEST"
    hash_key = "LockID"
    attribute {
        name = "LockID"
        type = "S"
    }
}