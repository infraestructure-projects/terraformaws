#!/bin/bash

#DEV
echo "Iniciando despliegue de Backend en dev"
cd ./aws-backend/environments/dev
terraform init 
terraform apply -auto-approve 

#echo "moviendo state de dev"
# mv ./terraform.tfstate ../../../aws/environments/dev/terraform.tfstate

echo "Iniciando despliegue de infraestructura en dev"
cd ../../../aws/environments/dev
terraform init 
terraform apply -auto-approve

#STAGE
echo "Iniciando despliegue de Backend en stage"
cd ../../../aws-backend/environments/stage
terraform init
terraform apply -auto-approve

echo "Iniciando despliegue de infraestructura en stage"
cd ../../../aws/environments/stage
terraform init
terraform apply -auto-approve

#PROD
echo "Iniciando despliegue de Backend en prod"
cd ../../../aws-backend/environments/prod
terraform init
terraform apply -auto-approve

echo "Iniciando despliegue de infraestructura en prod"
cd ../../../aws/environments/prod
terraform init
terraform apply -auto-approve




