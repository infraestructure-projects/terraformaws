variable "subnet_ids" {
  description = "The subnet IDs for the load balancer"
  type = list(string)
}
variable "lb_depends_on" {
  description = "The resources that the load balancer depends on"
}

variable "vpc_id" {
  description = "The VPC ID"
  type = string
}

variable "environment" {
  description = "The environment"
  type = string
}