#load balancer
resource "aws_lb" "j_lb" {
  name               = "j-lb-asg-${var.environment}" #load balancer auto scaling group
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.j_sg_lb.id]
  subnets            = var.subnet_ids
  depends_on         = [var.lb_depends_on] 
}

#target group
resource "aws_lb_target_group" "j_tg" {
  name     = "j-tg-asg-${var.environment}" #target group auto scaling group
  port     = 80
  protocol = "HTTP" 
  vpc_id   = var.vpc_id

  health_check {
    enabled = true
    protocol               = "HTTP"
    path                   = "/"
    port                   = 80
    interval               = 30
    timeout                = 5
    healthy_threshold      = 2
    unhealthy_threshold    = 2
    matcher                = "200"
  }
  depends_on = [aws_lb.j_lb]
}

#listener
resource "aws_lb_listener" "j_listener" {
  load_balancer_arn = aws_lb.j_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.j_tg.arn
    type             = "forward"
  }

  tags = {
    Name = "j-lb-listener-${var.environment}"
  }
}

#security group
resource "aws_security_group" "j_sg_lb" {
  name   = "j-sg-lb-${var.environment}"
  vpc_id = var.vpc_id
  
  ingress {
    description      = "Allow http request from anywhere"
    protocol         = "tcp"
    from_port        = 80
    to_port          = 80
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"] 
  }
  
  ingress {
    description      = "Allow https request from anywhere"
    protocol         = "tcp"
    from_port        = 443
    to_port          = 443
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  } 

}