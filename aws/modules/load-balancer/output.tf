output "target_group_arn" {
  value = aws_lb_target_group.j_tg.arn
}

output "lb_sg_id" {
  value = aws_security_group.j_sg_lb.id
}