#ASG with Launch Template
resource "aws_launch_template" "j_ec2_lt" {
    name_prefix = "j-ec2-lt-${var.environment}"
    image_id = var.ami
    instance_type = var.instance_type
    user_data = filebase64(var.user_data)
   # key_name = aws_key_pair.j-ssh-keys.key_name

    network_interfaces {
      associate_public_ip_address = false
      subnet_id = var.subnet_id
      security_groups = [aws_security_group.j_sg_ec2.id ]#, aws_security_group.j_sg_ssh.id]
    }

    tag_specifications {
        resource_type = "instance" 
        tags = {
            Name = "j-ec2-asg-${var.environment}"
        }
    }
}

  
# resource "aws_key_pair" "j-ssh-keys" {
#   key_name = "j-ssh-keys-${var.environment}"
#   public_key = file("/home/joaquinsolari/terraformaws/aws/modules/auto-scaling-group/keys/id_rsa.pub")
# }

resource "aws_autoscaling_group" "j_asg" {
    desired_capacity = var.desired_capacity
    max_size = var.max_size
    min_size = var.min_size
    name = "j-asg-${var.environment}"

  # Connect to the target group
  target_group_arns = [var.target_group_arn] 

  vpc_zone_identifier = [ # Creating EC2 instances in private subnet
    var.subnet_id
  ]

  launch_template {
    id      = aws_launch_template.j_ec2_lt.id
    version = "$Latest"
  }
}

resource "aws_security_group" "j_sg_ec2" {
  name   = "j-sg-ec2-${var.environment}"
  vpc_id = var.vpc_id

  ingress {
    description     = "Allow http request from Load Balancer"
    protocol        = "tcp"
    from_port       = 80 # range of
    to_port         = 80 # port numbers
    security_groups = [var.lb_sg_id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# resource "aws_security_group" "j_sg_ssh" {
#   name        = "j-sg-ssh-${var.environment}"
#   vpc_id      = var.vpc_id

#   ingress {
#     description = "Allow SSH from anywhere"
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#        }
# }

resource "aws_cloudwatch_metric_alarm" "j_ec2_cpu_alarm" {
  alarm_name          = "j-ec2-cpu-alarm-${var.environment}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2" # Number of periods to evaluate the condition 
  metric_name         = "CPUUtilization" 
  namespace           = "AWS/EC2" 
  period              = "60"
  statistic           = "Average"
  threshold           = "80"
  alarm_description   = "This metric controls instance scale up and down"
  dimensions = {
    AutoScalingGroupName = "${aws_autoscaling_group.j_asg.name}"
  }
}

