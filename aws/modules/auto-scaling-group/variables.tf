variable "ami" {
    description = "AMI ID"
    type        = string
    default = "ami-0454207e5367abf01"
}

variable "instance_type" {
    description = "Instance type"
    type        = string
    default = "t2.micro"
}

variable "user_data" {
    description = "User data"
    type        = string
}

variable "subnet_id" { 
    description = "Subnet ID"
    type        = string
}

variable "vpc_id" {
    description = "VPC ID"
    type        = string
}
variable "desired_capacity" {
    description = "Desired capacity"
    type        = number
    default = 1
}

variable "max_size" {
    description = "Max size"
    type        = number
    default = 1
}

variable "min_size" {
    description = "Min size"
    type        = number
    default = 1
}

variable "target_group_arn" {
    description = "The ARN of the target group"
    type        = string
  
}

variable "lb_sg_id" {
    description = "The security group ID of the load balancer"
    type        = string
}

variable "environment" {
    description = "Environment"
    type        = string
}