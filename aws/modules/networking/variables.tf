variable "vpc_cidr" {
  description = "The CIDR block for the VPC"
}

variable "public_subnet_1_cidr" {
  description = "The CIDR block for the public subnet 1"
}

variable "public_subnet_2_cidr" {
  description = "The CIDR block for the public subnet 2"
}

variable "private_subnet_cidr" {
  description = "The CIDR block for the private subnet "
}

variable "availability_zone_a" {
  description = "The availability zone for the VPC"
  default = "us-west-1b"
}

variable "availability_zone_b" {
  description = "The availability zone for the VPC"
    default = "us-west-1c"
}

variable "environment" {
  description = "The environment"
  type = string
}
