output "vpc_id" {
    value = aws_vpc.j-vpc.id
}

output "igw" {
    value = aws_internet_gateway.j-igw
}

output "public_subnet_1_id" {
    value = aws_subnet.j-public-subnet-1.id
}

output "public_subnet_2_id" {
    value = aws_subnet.j-public-subnet-2.id
  
}

output "private_subnet_id" {
    value = aws_subnet.j-private-subnet.id
}
