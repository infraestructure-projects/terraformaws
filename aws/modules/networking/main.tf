resource "aws_vpc" "j-vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = "j-vpc-${var.environment}"
  }
}

#public subnet 1
resource "aws_subnet" "j-public-subnet-1" {
  vpc_id = aws_vpc.j-vpc.id
  cidr_block = var.public_subnet_1_cidr
  map_public_ip_on_launch = true
  availability_zone = var.availability_zone_a  
  tags = {
    Name = "j-public-subnet-1-${var.environment}"
  }
}

#public subnet 2
resource "aws_subnet" "j-public-subnet-2" {
  vpc_id = aws_vpc.j-vpc.id
  cidr_block = var.public_subnet_2_cidr
  map_public_ip_on_launch = true
  availability_zone = var.availability_zone_b
  tags = {
    Name = "j-public-subnet-2-${var.environment}"
  }
}

#private subnet
resource "aws_subnet" "j-private-subnet" {
  vpc_id = aws_vpc.j-vpc.id
  cidr_block = var.private_subnet_cidr
  availability_zone = var.availability_zone_b
  map_public_ip_on_launch = false
  tags = {
    Name = "j-private-subnet-${var.environment}"
  }
}

#internet gateway
resource "aws_internet_gateway" "j-igw" {
  vpc_id = aws_vpc.j-vpc.id
  tags = {
    Name = "j-igw-${var.environment}"
  }
}

#route table
resource "aws_route_table" "j-rt-public" {
  vpc_id = aws_vpc.j-vpc.id
  
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.j-igw.id
    }
  
  tags = {
    Name = "j-rt-public-${var.environment}"
  }
}

#route table association
resource "aws_route_table_association" "j-rta1" {
  subnet_id = aws_subnet.j-public-subnet-1.id
  route_table_id = aws_route_table.j-rt-public.id
}

resource "aws_route_table_association" "j-rta2" {
  subnet_id = aws_subnet.j-public-subnet-2.id
  route_table_id = aws_route_table.j-rt-public.id
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#Elasitc IP
resource "aws_eip" "j-eip" {
    domain = "vpc"
    depends_on = [aws_internet_gateway.j-igw]
    tags = {
         Name = "j-eip-${var.environment}"
           }
}

#NAT Gateway --For private subnets to reach internet(to download updates, etc.)
resource "aws_nat_gateway" "j-ngw" {
  allocation_id = aws_eip.j-eip.id
  subnet_id = aws_subnet.j-public-subnet-1.id # nat should be in public subnet
  depends_on = [aws_internet_gateway.j-igw]
  tags = {
    Name = "j-ngw-${var.environment}"
         }
}

#route table for private subnet
resource "aws_route_table" "j-rt-private" {
  vpc_id = aws_vpc.j-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.j-ngw.id 
    }
  
  tags = {
    Name = "j-rt-private-${var.environment}"
  }
}

#route table association for private subnet
resource "aws_route_table_association" "j-rta3" {
  subnet_id = aws_subnet.j-private-subnet.id
  route_table_id = aws_route_table.j-rt-private.id
}

