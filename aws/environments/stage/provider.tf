terraform {
    required_providers {
        aws = {
        source  = "hashicorp/aws"
        version = "5.40.0"
        }
    }

    backend "s3" {
        bucket = "tf-state-bucket-j-stg"
        key    = "stg/terraform.tfstate"
        region = "us-west-1"
        dynamodb_table = "terraform-state-locking-j-stg"
        encrypt = true
    }
}

provider "aws" {
    region = "us-west-1"
}
