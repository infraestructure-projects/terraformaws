module "networking" {
  source = "../../modules/networking"
  vpc_cidr = "10.2.0.0/16"
  environment = "stage"
  public_subnet_1_cidr = "10.2.0.0/24"
  public_subnet_2_cidr = "10.2.1.0/24"
  private_subnet_cidr = "10.2.2.0/24"
}

module "load_balancer" {
    source = "../../modules/load-balancer"
    environment = "stg"
    vpc_id = module.networking.vpc_id
    lb_depends_on = module.networking.igw
    subnet_ids = [module.networking.public_subnet_1_id, module.networking.public_subnet_2_id]
}

module "auto_scaling_group" {
    source = "../../modules/auto-scaling-group"
    environment = "stage"
    vpc_id = module.networking.vpc_id
    subnet_id = module.networking.private_subnet_id
    target_group_arn = module.load_balancer.target_group_arn
    user_data = "../../../user_data.sh"
    lb_sg_id = module.load_balancer.lb_sg_id
    #other variables with default values
}